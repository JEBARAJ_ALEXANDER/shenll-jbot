#!/bin/bash
docker pull javashenll/jade-wallet-parse
wait
docker ps | awk '{ print $1,$2 }' | grep -w "javashenll/jade-wallet-parse$" | awk '{ print $1 }' | xargs -r docker stop
wait
docker ps -a | awk '{ print $1,$2 }' | grep -w "javashenll/jade-wallet-parse$" | awk '{ print $1 }' | xargs -r docker rm
wait
docker run -d -p 1338:1337 javashenll/jade-wallet-parse