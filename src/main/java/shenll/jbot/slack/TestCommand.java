package shenll.jbot.slack;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import me.ramswaroop.jbot.core.slack.models.Attachment;
import me.ramswaroop.jbot.core.slack.models.RichMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


@RestController
@Profile("slack")
public class TestCommand {

    private static final Logger logger = LoggerFactory.getLogger(TestCommand.class);


    @RequestMapping(value = "/test-command", method = RequestMethod.GET)
    public String testCommand(@RequestParam("text") String text) {
        Command cf = CommandFactory.build(text);
        String result = cf.execute();
        return result;
    }
}
