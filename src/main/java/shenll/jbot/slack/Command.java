package shenll.jbot.slack;

public interface Command {
    String execute();
}
