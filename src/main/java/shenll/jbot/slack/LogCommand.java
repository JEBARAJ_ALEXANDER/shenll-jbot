package shenll.jbot.slack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LogCommand implements Command {
     String input;
    public LogCommand(String input) {
        this.input = input.replace("log ", "");
    }

    @Override
   public String execute(){
        StringBuilder response = new StringBuilder();
        try {
            Process pwd = Runtime.getRuntime().exec("pwd");
            pwd.waitFor();
            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(pwd.getInputStream()));
            String cmd = String.format("sh %s/scripts/log-%s.sh", stdInput.readLine(), input);
            System.out.println("cmd : " + cmd);
            System.out.println("input : " + input);
            Process proc = Runtime.getRuntime().exec(cmd);
            proc.waitFor();

            BufferedReader logOut = new BufferedReader(new
                    InputStreamReader(proc.getInputStream()));

            String line;
            while ((line = logOut.readLine()) != null) {
                response.append(line);
            }
        } catch (InterruptedException e) {
            return "Deployment Failed!";
        } catch (IOException e) {
            return "Deployment Failed!";
        }
        return response.toString();
    }
}
