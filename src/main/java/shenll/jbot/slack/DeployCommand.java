package shenll.jbot.slack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class DeployCommand implements Command {

    private final String input;

    public DeployCommand(String input) {
        this.input = input.replace("deploy ", "");
    }

    @Override
    public String execute() {
        try {
            Process pwd = Runtime.getRuntime().exec("pwd");
            pwd.waitFor();
            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(pwd.getInputStream()));
            String cmd = String.format("sh %s/scripts/deploy-%s.sh", stdInput.readLine(), input);
            System.out.println("cmd : " + cmd);
            System.out.println("input : " + input);
            Process proc = Runtime.getRuntime().exec(cmd);
            proc.waitFor();
        } catch (InterruptedException e) {
            return "Deployment Failed!";
        } catch (IOException e) {
            return "Deployment Failed!";
        }
        return "Deployment Successful!";
    }
}
