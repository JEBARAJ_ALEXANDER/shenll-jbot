package shenll.jbot.slack;

class CommandFactory {
    static Command build(String input) {
        if (input == null) return new HelpCommand();
        String i = input.trim().toLowerCase();
        if (i.contains("deploy")) return new DeployCommand(i);
        if (i.contains("log")) return new LogCommand(i);
        return new HelpCommand();
    }
}
